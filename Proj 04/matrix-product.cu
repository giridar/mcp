#include "cuda_runtime.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctime>

void write_to_file(const char* filename, int *a, int r, int c) {
   // write contents of matrix to file
   FILE *fp = fopen(filename, "w");
   for(int i = 0; i < r; i++) {
      for(int j = 0; j < c; j++)
         fprintf(fp, "%d ", a[i * c + j]);
      fprintf(fp, "\n");
   }  
}

void init_rand(int *a, int r, int c) {
   // initialize matrix with random integers less than 10
   for(int i = 0; i < r; i++) {
      for(int j = 0; j < c; j++)
         a[i * c + j] = rand() % 10;
   }
}

void cpu_product(int *a, int *b, int *c, int r_a, int c_a, int r_b, int c_b) {
   // calculate the matrix product
   printf("Calculating matrix product on CPU...\n");
   int start = clock();
   for(int i = 0; i < r_a; i++) {
      for(int j = 0; j < c_b; j++) {
         for(int k = 0; k < c_a; k++)
            c[i * c_b + j] += a[i * c_a + k] * b[k * c_b + j];
      }
   }
   int end = clock();
   printf("Time taken: %.3f s\n", (double)(end - start) / CLOCKS_PER_SEC);

   // write the product to file
   printf("Writing result matrix to file\n");
   write_to_file("cpu-product.txt", c, r_a, c_b);
}

__global__
void kernel1(int *a, int *b, int *c, int r_a, int c_a, int r_b, int c_b) {
   // one thread per sum of products
   for(int k = 0; k < c_a; k++)
      c[blockIdx.x * c_b + blockIdx.y] += a[blockIdx.x * c_a + k] * b[k * c_b + blockIdx.y];
}

__global__
void kernel2(int *a, int *b, int *c, int r_a, int c_a, int r_b, int c_b) {
   // one thread per product
   int prod = a[blockIdx.x * c_a + threadIdx.x] * b[threadIdx.x * c_b + blockIdx.y];
   atomicAdd(c + (blockIdx.x * c_b + blockIdx.y), prod);
}

void gpu_product(int *a, int *b, int *c, int size_a, int size_b, int size_cde, int r_a, int c_a, int r_b, int c_b, int o) {
   // allocate device copies of a, b, c
   int *dev_a, *dev_b, *dev_c;   
   cudaMalloc((void **) &dev_a, size_a);
   cudaMalloc((void **) &dev_b, size_b);
   cudaMalloc((void **) &dev_c, size_cde);

   // copy inputs to device memory
   printf("Copying matrices to device memory\n");
   cudaMemcpy(dev_a, a, size_a, cudaMemcpyHostToDevice);
   cudaMemcpy(dev_b, b, size_b, cudaMemcpyHostToDevice);
   cudaMemcpy(dev_c, c, size_cde, cudaMemcpyHostToDevice);

   // launch kernel on GPU, passing parameters
   dim3 blocks(r_a, c_b);
   int threads = c_a;
   printf("Calculating matrix product on GPU...\n");
   int start = clock();
   if(o == 1)
      kernel1<<<blocks, 1>>>(dev_a, dev_b, dev_c, r_a, c_a, r_b, c_b);
   else
      kernel2<<<blocks, threads>>>(dev_a, dev_b, dev_c, r_a, c_a, r_b, c_b);
   int end = clock();
   printf("Time taken: %.3f s\n", (double)(end - start) / CLOCKS_PER_SEC);

   // copy device result back to host copy of c
   printf("Copying back result matrix\n");
   cudaMemcpy(c, dev_c, size_cde, cudaMemcpyDeviceToHost);

   // write the product to file
   printf("Writing result matrix to file\n");
   char filename[17];
   sprintf(filename, "gpu-product-%d.txt", o);
   write_to_file(filename, c, r_a, c_b);

   // deallocate device copies of a, b, c
   cudaFree(dev_a); 
   cudaFree(dev_b);
   cudaFree(dev_c);
}

int test_results(int *c, int *d, int size_cde) {
   for(int i = 0; i < size_cde; i++) {
      if(c[i] != d[i])
         return 0;
   }
   return 1;
}

int main(int argc, char *argv[]) {
   // check number of arguments
   if(argc < 4) {
      printf("Error: incorrect number of arguments");
      return 1;
   }

   // host copies of the matrices
   int *a, *b, *c, *d, *e;
   // dimensions of the matrices
   int r_a = atoi(argv[1]), c_a = atoi(argv[2]), r_b = atoi(argv[2]), c_b = atoi(argv[3]);

   // size of the matrices
   int size_a = r_a * c_a * sizeof(int);
   int size_b = r_b * c_b * sizeof(int);
   int size_cde = r_a * c_b;

   // allocate host copies of a, b, c, d
   a = (int *) malloc(size_a);
   b = (int *) malloc(size_b);
   c = (int *) calloc(size_cde, sizeof(int));
   d = (int *) calloc(size_cde, sizeof(int));
   e = (int *) calloc(size_cde, sizeof(int));

   // initialize host copies of a, b
   printf("Initializing matrices\n");
   init_rand(a, r_a, c_a);
   init_rand(b, r_b, c_b);

   // calculate matrix product using CPU computation
   cpu_product(a, b, c, r_a, c_a, r_b, c_b);

   // calculate matrix product using GPU computation 1
   gpu_product(a, b, d, size_a, size_b, size_cde * sizeof(int), r_a, c_a, r_b, c_b, 1);

   // calculate matrix product using GPU computation 2
   gpu_product(a, b, e, size_a, size_b, size_cde * sizeof(int), r_a, c_a, r_b, c_b, 2);

   // test the results
   if(!test_results(c, d, size_cde))
      printf("Error: result value mismatch in GPU 1\n");
   if(!test_results(c, e, size_cde))
      printf("Error: result value mismatch in GPU 2\n");

   // deallocate host copies of a, b, c
   free(a);
   free(b);
   free(c);
   free(d);
   free(e);

   printf("Done!\n");
   return 0;
}