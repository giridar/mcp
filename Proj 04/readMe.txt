Multicore Programming - Project 04
__________________________________

Contents
--------
* matrix-product.cu - CUDA source file to run the experiments

Command
-------
nvcc matrix-product.cu
./a.out s1 s2 s3

(s1, s2) - dimensions of matrix A
(s2, s3) - dimensions of matrix B

ex., ./a.out 512 512 512
