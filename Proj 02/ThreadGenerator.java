/**
 * Created by Srikanth on 10/23/2016.
 */
public class ThreadGenerator {
    private IConcurrentList iConcurrentList;
    private static int phase = 0;
    private static long startTime, endTime, elapsedTime;

    public ThreadGenerator() {
        iConcurrentList = ApplicationRunner.getConcurrentLinkedList() == ConcurrentLinkedList.COARSEGRAINED ?
                new CoarseGrainedList() : ApplicationRunner.getConcurrentLinkedList() == ConcurrentLinkedList.FINEGRAINED ?
                new FineGrainedList() : new LockFreeList();
    }

    public void start() {
        try {
            Thread[] threads = new Thread[ApplicationRunner.getTotalThreads()];
            timer();
            // start application threads
            for (int i = 0; i < threads.length; i++) {
                threads[i] = new Thread(new ApplicationThread(this));
                threads[i].start();
            }

            for (Thread thread : threads)
                thread.join();
            timer();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public IConcurrentList getConcurrentList() {
        return iConcurrentList;
    }

    /**
     * Timer to calculate the running time
     */
    public static void timer() {
        if (phase == 0) {
            startTime = System.currentTimeMillis();
            phase = 1;
        } else {
            endTime = System.currentTimeMillis();
            elapsedTime = endTime - startTime;
            System.out.println("Time: " + elapsedTime + " msec.");
            memory();
            phase = 0;
        }
    }

    /**
     * This method determines the memory usage
     */
    public static void memory() {
        long memAvailable = Runtime.getRuntime().totalMemory();
        long memUsed = memAvailable - Runtime.getRuntime().freeMemory();
        System.out.println("Memory: " + memUsed / 1000000 + " MB / " + memAvailable / 1000000 + " MB.");
    }
}
