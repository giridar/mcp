/**
 * Created by Srikanth on 10/23/2016.
 */
public class ApplicationRunner {
    private static ConcurrentLinkedList concurrentLinkedList;
    private static int totalThreads;

    public static void main(String[] args) {
        if (args.length != 1)
            System.out.print("Invalid Arguments passed. Please pass single argument which tells number of threads to be used.");
        try {
            totalThreads = Integer.parseInt(args[0]);
            concurrentLinkedList = ConcurrentLinkedList.COARSEGRAINED;
            ThreadGenerator threadGenerator = new ThreadGenerator();
            threadGenerator.start();

            concurrentLinkedList = ConcurrentLinkedList.FINEGRAINED;
            threadGenerator = new ThreadGenerator();
            threadGenerator.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static ConcurrentLinkedList getConcurrentLinkedList() {
        return concurrentLinkedList;
    }

    public static int getTotalThreads() {
        return totalThreads;
    }
}

enum ConcurrentLinkedList {
    COARSEGRAINED, FINEGRAINED, LOCKFREE
}
