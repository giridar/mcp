/**
 * Created by Srikanth&Giri on 10/23/2016.
 */
public interface IConcurrentList {
    boolean add(int key);

    boolean remove(int key);

    boolean contains(int key);
}
