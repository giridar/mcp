/**
 * Created by Srikanth&Giri on 10/23/2016.
 */
public class ApplicationThread implements Runnable {

    private ThreadGenerator threadGenerator;

    public ApplicationThread(ThreadGenerator threadGenerator) {
        this.threadGenerator = threadGenerator;
    }

    @Override
    public void run() {
        // TODO need to improvise this section based on 90% search and 90% add/remove
        threadGenerator.getConcurrentList().add(10);
        threadGenerator.getConcurrentList().add(20);
        threadGenerator.getConcurrentList().add(30);
        threadGenerator.getConcurrentList().add(40);
        threadGenerator.getConcurrentList().add(50);
        threadGenerator.getConcurrentList().remove(40);
        threadGenerator.getConcurrentList().remove(30);
        threadGenerator.getConcurrentList().contains(30);
        threadGenerator.getConcurrentList().contains(20);
    }
}
