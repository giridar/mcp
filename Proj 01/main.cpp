#include <iostream>
#include <cstdlib>
#include <thread>
#include <atomic>
#include <math.h>
#include <ctime>
using namespace std;

/* Abstract lock class */
class abs_lock {
public:
	virtual void lock(int id) = 0;
	virtual void unlock(int id) = 0;
};

/* TAS lock */
class tas : public abs_lock {
public:
	atomic<bool> flag;

public:
	tas() {
		flag = new atomic<bool>(false);
		flag.store(false);
	}

	void lock(int id) {
		while (flag.exchange(true));
	}

	void unlock(int id) {
		flag.store(false);
	}
};

/* TTAS lock */
class ttas : public abs_lock {
private:
	atomic<bool> flag;

public:
	ttas() {
		flag = new atomic<bool>(false);
		flag.store(false);
	}

	void lock(int id) {
		while (1) {
			while (flag.load());
			if (!flag.exchange(true))
				return;
		}
	}

	void unlock(int id) {
		flag.store(false);
	}
};

/* Peterson lock */
class peterson : public abs_lock {
private:
	atomic<bool>* flag;
	atomic<int> victim;

public:
		peterson() {
				flag = new atomic<bool>[2]();
		}

		void lock(int id) {
				flag[id] = true;
				victim = id;
		while (flag[1-id] && victim == id) ;
	}

	void unlock(int id) {
				flag[id] = false;
	}
};

/* Tournament lock */
class tournament : public abs_lock {
private:
	peterson* locks;
		int k;
		int n;

public:
	tournament(int count) {
				k = ceil(log2(count));
				n = 1 << k;
				locks = new peterson[n]();
	}

	void lock(int id) {
				int i = id + n;
				for (int l = 0; l < k; l++) {
//                         cout << "Lock " << i << endl;
						locks[i / 2].lock(i % 2);
						i = i / 2;
				}
	}

	void unlock(int id) {
				id = id + n;
				int i = 0;
				for (int l = k - 1; l >= 0; l--) {
						i = id >> l;
//                         cout << "Unlock " << i << endl;
						locks[i / 2].unlock(i % 2);
				}
	}
};

unsigned long long int counter = 0; // shared resource
int count; // no. of CS executions
abs_lock* lock; // pointer to lock object

/* Thread function */
void increment(int id) {
	for (int i = 0; i < count; i++) {
//                 cout << "Thread " << id << ": lock()" << endl;
		lock->lock(id);
//                 cout << "Thread " << id << ": cs()" << endl;
		counter++; // Critical section
//                 cout << "Thread " << id << ": unlock()" << endl;
		lock->unlock(id);
	}
// 	cout << counter << endl;
}

int main(int argc, char* argv[]) {
	/* Pre-process the IP args */
	int n = atoi(argv[1]);
	count = atoi(argv[2]);
	switch (argv[3][0]) {
				case '1':
				lock = new tournament(n);
				break;

				case '2':
		lock = new tas();
		break;

		case '3':
		lock = new ttas();
		break;
	}

	/* Create threads */
	thread t[n];
	int i, rc;
		int start = clock();
	for (i = 0; i < n; i++)
		t[i] = thread(increment, i);

	/* Wait for the threads to complete */
	for (i = 0; i < n; i++)
		t[i].join();
		int end = clock();
		cout << "Time taken : " << 1000 * double(end - start) / CLOCKS_PER_SEC << " ms" << endl;
		cout << "Final count: "counter << endl;

	delete lock;
	/* Test final result */
	if (counter == n * count)
		return 0;
	else
		return 1;
}


