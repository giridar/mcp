Multicore Programming - Project 01
__________________________________

Contents
--------
* main.cpp - C++ source file to run the experiments

Command
-------
g++ -pthread -std=c++11 main.cpp
./a.out n c o

n - number of threads
c - number of times to enter the CS per thread
o - the lock option
    1. Tournament lock
    2. TAS lock
    3. TTAS lock

ex., ./a.out 4 1000000 3
